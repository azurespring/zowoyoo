<?php

namespace AzureSpring\Zowoyoo\Notification;

class UpdateNotification extends OrderNotification
{
    const STATE_APPROVED = 1;
    const STATE_REJECTED = 2;

    /** @var string */
    private $referenceId;

    /** @var bool */
    private $approved;

    /** @var int */
    private $quantity;

    /** @var float */
    private $subtotal;

    /** @var string */
    private $reason;

    public static function support(array $params)
    {
        return !array_diff(['order_id', 'apply_no', 'audi_state'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self(
            $params['order_id'],
            $params['apply_no'],
            self::STATE_APPROVED === (int) $params['audi_state'],
            $params['cancel_num'],
            $params['cancel_money'],
            @$params['audi_memo'] ?? ''
        );
    }

    public function __construct(string $orderId, string $referenceId, bool $approved, int $quantity, float $subtotal, string $reason)
    {
        parent::__construct($orderId);

        $this->referenceId = $referenceId;
        $this->approved = $approved;
        $this->quantity = $quantity;
        $this->subtotal = $subtotal;
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->subtotal;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }
}
