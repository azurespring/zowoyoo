<?php

namespace AzureSpring\Zowoyoo\Notification;

class ProductNotification extends AbstractNotification
{
    const TYPE_PRICE = 1;
    const TYPE_INFO = 2;

    /** @var string[] */
    private $productIds;

    /** @var int */
    private $type;

    public static function support(array $params)
    {
        return !array_diff(['info_ids', 'type'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self(array_map('trim', explode(',', $params['info_ids'])), $params['type']);
    }

    public function __construct(array $productIds, int $type)
    {
        $this->productIds = $productIds;
        $this->type = $type;
    }

    /**
     * @return string[]
     */
    public function getProductIds(): array
    {
        return $this->productIds;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }
}
