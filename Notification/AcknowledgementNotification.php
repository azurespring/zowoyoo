<?php

namespace AzureSpring\Zowoyoo\Notification;

class AcknowledgementNotification extends OrderNotification
{
    /** @var int */
    private $quantity;

    /** @var float */
    private $subtotal;

    public static function support(array $params)
    {
        return !array_diff(['order_id', 'ticket_num', 'order_money'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self($params['order_id'], $params['ticket_num'], $params['order_money']);
    }

    public function __construct(string $orderId, int $quantity, float $subtotal)
    {
        parent::__construct($orderId);

        $this->quantity = $quantity;
        $this->subtotal = $subtotal;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->subtotal;
    }
}
