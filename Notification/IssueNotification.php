<?php

namespace AzureSpring\Zowoyoo\Notification;

class IssueNotification extends OrderNotification
{
    const TYPE_PDF = 0;
    const TYPE_IMG = 1;

    const CTYPE_TXT = 0;
    const CTYPE_IMG = 1;
    const CTYPE_PDF = 2;
    const CTYPE_URL = 3;

    /** @var string */
    private $id;

    /** @var bool */
    private $ok;

    /** @var string */
    private $code;

    /** @var string[] */
    private $urls;

    /** @var int */
    private $type;

    /** @var array */
    private $codeVector;

    public static function support(array $params)
    {
        return !array_diff(['order_id', 'msg_id', 'printState'], array_keys($params));
    }

    public static function compose(array $params)
    {
        if ('0' !== (string) $params['printState']) {
            return new self($params['msg_id'], $params['order_id'], false, '', [], -1, []);
        }

        return new self(
            $params['msg_id'],
            $params['order_id'],
            true,
            $params['code'],
            array_filter(array_map('trim', explode(',', $params['qrCodeUrl']))),
            (int) $params['qrType'],
            array_reduce(
                \GuzzleHttp\json_decode($params['codeUrls'], true),
                function ($v, $i) {
                    return $v + [$i['codeUrl'] => $i['codeType']];
                },
                []
            )
        );
    }

    public function __construct(string $id, string $orderId, bool $ok, string $code, array $urls, int $type, array $codeVector)
    {
        parent::__construct($orderId);

        $this->id = $id;
        $this->ok = $ok;
        $this->code = $code;
        $this->urls = $urls;
        $this->type = $type;
        $this->codeVector = $codeVector;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isOk(): bool
    {
        return $this->ok;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string[]
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getCodeVector(): array
    {
        return $this->codeVector;
    }
}
