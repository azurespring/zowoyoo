<?php

namespace AzureSpring\Zowoyoo\Notification;

class CancellationNotification extends OrderNotification
{
    /** @var int */
    private $quantity;

    public static function support(array $params)
    {
        return !array_diff(['order_id', 'cancel_num'], array_keys($params));
    }

    public static function compose(array $params)
    {
        return new self($params['order_id'], $params['quantity']);
    }

    public function __construct(string $orderId, int $quantity)
    {
        parent::__construct($orderId);

        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
