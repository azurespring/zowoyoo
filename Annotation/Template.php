<?php

namespace AzureSpring\Zowoyoo\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class Template
{
    /**
     * @var array<string>
     * @Required
     */
    public $params;

    public static function bind(string $type, string ...$params)
    {
        return sprintf('template<%s<%s>>', $type, implode(', ', $params));
    }
}
