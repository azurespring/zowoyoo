<?php

namespace AzureSpring\Zowoyoo;

use AzureSpring\Zowoyoo\Annotation\Template;
use AzureSpring\Zowoyoo\Model;
use AzureSpring\Zowoyoo\Notification;
use AzureSpring\Zowoyoo\Serializer\CsvHandler;
use AzureSpring\Zowoyoo\Serializer\TemplateHandler;
use AzureSpring\Zowoyoo\Serializer\UnitHandler;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class Client
{
    private $guzzle;

    private $key;

    private $secret;

    private $serializer;

    private $responseFactory;

    private $streamFactory;

    private $contents;

    public function __construct(\GuzzleHttp\Client $guzzle, string $key, string $secret, ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory)
    {
        $this->guzzle = $guzzle;
        $this->key = $key;
        $this->secret = $secret;
        $this->serializer = SerializerBuilder::create()
            ->setMetadataDirs([
                'AzureSpring\\Zowoyoo\\Model' => __DIR__.'/Resources/config/serializer',
            ])
            ->configureHandlers(function (HandlerRegistryInterface $registry) {
                $registry->registerSubscribingHandler(new TemplateHandler());
                $registry->registerSubscribingHandler(new UnitHandler());
                $registry->registerSubscribingHandler(new CsvHandler());
            })
            ->addDefaultHandlers()
            ->build()
        ;

        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
    }

    public function findProducts(Model\ProductFilter $filter, int $size = 50, int $page = 1, ?int $orderBy = null): Model\Fragment
    {
        return $this->request(
            Template::bind(Model\Fragment::class, Model\ProductRef::class),
            'GET',
            '/api/list.jsp',
            array_merge($filter->toParams(), [
                'pageNum' => $size,
                'pageNo' => $page,
                'orderBy' => $orderBy,
            ]),
            ['products']
        );
    }

    public function findProduct(string $id): Model\Product
    {
        return $this
            ->request(
                Model\Unit::bind(Model\Product::class, 'product'),
                'GET',
                '/api/detail.jsp',
                ['productNo' => $id]
            )
            ->prune()
        ;
    }

    public function save(Model\OrderOptions $options): Model\Order
    {
        return $this->request(
            Model\Unit::bind(Model\Order::class),
            'POST',
            '/api/order.jsp',
            [
                'order' => $options->toParams() + [
                    'cust_id' => $this->key,
                ],
            ]
        );
    }

    public function clear(string $id)
    {
        $this->request(Model\Unit::class, 'GET', '/api/pay.jsp', ['orderId' => $id]);
    }

    public function update(string $id, int $loss, ?string $reason = null): string
    {
        return $this->request(Model\Unit::bind('string', 'apply_no'), 'GET', '/api/changeApplyOrder.jsp', [
            'orderId' => $id,
            'refundNum' => $loss,
            'changeMemo' => $reason,
        ]);
    }

    public function cancel(string $id, ?int $quantity = null)
    {
        if (!$quantity) {
            $this->request(Model\Unit::class, 'GET', '/api/cancelOrder.jsp', [
                'orderId' => $id,
            ]);
        } else {
            $this->request(Model\Unit::class, 'GET', '/api/refundOrder.jsp', [
                'orderId' => $id,
                'num' => $quantity,
            ]);
        }
    }

    public function sms(string $id, string $mobile)
    {
        $this->request(Model\Unit::class, 'GET', '/api/resend.jsp', [
            'orderId' => $id,
            'mobile' => $mobile,
        ]);
    }

    public function recv(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        if ($this->key !== $params['cust_id'] || $this->secret !== $params['apikey']) {
            return null;
        }

        /** @var Notification\AbstractNotification $class */
        foreach ([
            Notification\AcknowledgementNotification::class,
            Notification\IssueNotification::class,
            Notification\UpdateNotification::class,
            Notification\VerificationNotification::class,
            Notification\CancellationNotification::class,
            Notification\ProductNotification::class,
        ] as $class) {
            if ($class::support($params)) {
                return $class::compose($params);
            }
        }

        return null;
    }

    public function ackn(): ResponseInterface
    {
        return $this
            ->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/xml')
            ->withBody($this->streamFactory->createStream(
                $this->xmlize('root', ['result' => 'true', 'msg' => '接收成功'], true)
            ))
        ;
    }

    public function getContents()
    {
        return $this->contents;
    }

    private function request(string $type, string $method, string $uri, array $params, array $serializationGroups = [])
    {
        $form = null;
        $query = [
            'custId' => $this->key,
            'apikey' => $this->secret,
        ];
        switch ($method) {
            case 'POST':
                $form = [
                    'param' => $this->xmlize(key($params), current($params)),
                ];
                break;

            default:
                $query += $params;
                break;
        }

        $response = $this->guzzle->request($method, $uri, ['query' => $query, 'form_params' => $form]);
        $this->contents = $response->getBody()->getContents();
        $context = DeserializationContext
            ::create()
            ->setGroups(array_merge(['Default'], $serializationGroups))
        ;

        /** @var Model\Result $result */
        $result = $this->serializer->deserialize($this->contents, Template::bind(Model\Result::class, $type), 'xml', $context);
        if (Model\Result::STATUS_OK !== $result->getStatus()) {
            throw new Exception($result->getMessage());
        }

        return $result->getData()->squeeze();
    }

    private function xmlize($name, $value, bool $prolog = false)
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->appendChild($element = $this->doXmlize($name, $value, $document));

        return $prolog ? $document->saveXML() : $document->saveXML($element);
    }

    private function doXmlize($name, $value, \DOMDocument $document)
    {
        if (!is_array($value)) {
            return $document->createElement($name, $value);
        }

        $element = $document->createElement($name);
        foreach ($value as $k => $v) {
            $element->appendChild($this->doXmlize($k, $v, $document));
        }

        return $element;
    }
}
