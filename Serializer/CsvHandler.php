<?php

namespace AzureSpring\Zowoyoo\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\XmlDeserializationVisitor;

class CsvHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_DESERIALIZATION,
                'format' => 'xml',
                'type' => 'csv',
                'method' => 'deserialize',
            ],
        ];
    }

    public function deserialize(XmlDeserializationVisitor $visitor, $data, array $type, Context $context)
    {
        $t = ['name' => 'string'];
        $d = ',';
        foreach ($type['params'] as $p) {
            if (is_array($p)) {
                $t = $p;
            } else {
                $d = $p;
            }
        }

        return array_map(
            function ($data) use ($t, $context) {
                return $context->getNavigator()->accept(simplexml_load_string("<v><![CDATA[{$data}]]></v>"), $t);
            },
            array_filter(array_map('trim', explode($d, $data)))
        );
    }
}
