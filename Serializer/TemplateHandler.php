<?php

namespace AzureSpring\Zowoyoo\Serializer;

use AzureSpring\Zowoyoo\Annotation\Template;
use Doctrine\Common\Annotations\AnnotationReader;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\XmlDeserializationVisitor;

class TemplateHandler implements SubscribingHandlerInterface
{
    private $frames = [];

    private $reader;

    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_DESERIALIZATION,
                'format' => 'xml',
                'type' => 'template',
                'method' => 'deserialize',
            ],
        ];
    }

    public function __construct()
    {
        $this->reader = new AnnotationReader();
    }

    public function deserialize(XmlDeserializationVisitor $visitor, $data, array $type, Context $context)
    {
        $types = end($this->frames) ?: [];
        $bind = function ($param) use (&$bind, $types) {
            if (!is_array($param)) {
                return $param;
            }
            if (array_key_exists($name = $param['name'], $types)) {
                return $types[$name];
            }

            return [
                'name' => $name,
                'params' => array_map($bind, $param['params']),
            ];
        };

        $type = $bind(current($type['params']));
        $frame = [];
        if (class_exists($type['name'])) {
            $class = new \ReflectionClass($type['name']);

            /** @var Template $template */
            if ($template = $this->reader->getClassAnnotation($class, Template::class)) {
                $frame = array_combine($template->params, $type['params']);
            }
        }

        $this->frames[] = $frame;
        $data = $context->getNavigator()->accept($data, $type);
        array_pop($this->frames);

        return $data;
    }
}
