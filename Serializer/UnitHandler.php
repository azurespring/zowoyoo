<?php

namespace AzureSpring\Zowoyoo\Serializer;

use AzureSpring\Zowoyoo\Model\Unit;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\XmlDeserializationVisitor;

class UnitHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_DESERIALIZATION,
                'format' => 'xml',
                'type' => 'unit',
                'method' => 'deserialize',
            ],
        ];
    }

    /**
     * @param \SimpleXMLElement $data
     */
    public function deserialize(XmlDeserializationVisitor $visitor, $data, array $type, Context $context)
    {
        if ($type['params'][1]) {
            $data = $data->{$type['params'][1]};
        }

        return new Unit($context->getNavigator()->accept($data, $type['params'][0]));
    }
}
