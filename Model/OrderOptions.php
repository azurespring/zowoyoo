<?php

namespace AzureSpring\Zowoyoo\Model;

class OrderOptions
{
    const TYPE_ID_CARD = 0;

    /** @var string|null */
    private $permanentId;

    /** @var \DateTimeImmutable|null */
    private $date;

    /** @var string|null */
    private $productId;

    /** @var int|null */
    private $quantity;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $phone;

    /** @var string|null */
    private $idNumber;

    public static function create()
    {
        return new OrderOptions();
    }

    /**
     * @return string|null
     */
    public function getPermanentId(): ?string
    {
        return $this->permanentId;
    }

    /**
     * @param string|null $permanentId
     *
     * @return $this
     */
    public function setPermanentId(?string $permanentId): OrderOptions
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @param \DateTimeImmutable|null $date
     *
     * @return $this
     */
    public function setDate(?\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductId(): ?string
    {
        return $this->productId;
    }

    /**
     * @param string|null $productId
     *
     * @return $this
     */
    public function setProductId(?string $productId): self
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     *
     * @return $this
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    /**
     * @param string|null $idNumber
     *
     * @return $this
     */
    public function setIdNumber(?string $idNumber): self
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    public function toParams()
    {
        return [
            'travel_date' => $this->getDate()->setTimezone(new \DateTimeZone('Asia/Shanghai'))->format('Y-m-d'),
            'info_id' => $this->getProductId(),
            'num' => $this->getQuantity(),
            'link_man' => $this->getName(),
            'link_phone' => $this->getPhone(),
            'link_credit_type' => self::TYPE_ID_CARD,
            'link_credit_no' => $this->getIdNumber(),
        ];
    }
}
