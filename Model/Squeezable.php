<?php

namespace AzureSpring\Zowoyoo\Model;

interface Squeezable
{
    public function squeeze();
}
