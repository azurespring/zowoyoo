<?php

namespace AzureSpring\Zowoyoo\Model;

use AzureSpring\Zowoyoo\Annotation\Template;

/** @Template({"T"}) */
class Fragment implements Squeezable
{
    /** @var int */
    private $total;

    private $data;

    protected function __construct(int $total, $data)
    {
        $this->total = $total;
        $this->data = $data;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getData()
    {
        return $this->data;
    }

    public function squeeze()
    {
        if (!$this->data instanceof Collection) {
            return $this;
        }

        return new Fragment($this->total, $this->data->getElements());
    }
}
