<?php

namespace AzureSpring\Zowoyoo\Model;

abstract class Validity
{
    const TYPE_RESERVATION_DATE = 0;
    const TYPE_DELAY_FROM_TODAY = 1;
    const TYPE_FROM_TODAY_THRU = 2;
    const TYPE_DELAY_FROM_RESERVATION = 3;
    const TYPE_FROM_RESERVATION_THRU = 4;
    const TYPE_SPECIFIC_PERIOD = 5;

    /** @var int */
    private $type;

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    public function getValidFrom(): ?\DateTimeImmutable
    {
        return null;
    }

    public function getValidThru(): ?\DateTimeImmutable
    {
        return null;
    }

    public function prune()
    {
        return $this;
    }
}
