<?php

namespace AzureSpring\Zowoyoo\Model;

use AzureSpring\Zowoyoo\Annotation\Template;

/** @Template({"T"}) */
class Result
{
    const STATUS_ERROR = 0;
    const STATUS_OK = 1;

    /** @var int */
    private $status;

    /** @var string|null */
    private $message;

    private $data;

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }
}
