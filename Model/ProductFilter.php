<?php

namespace AzureSpring\Zowoyoo\Model;

class ProductFilter
{
    const ORDER_BY_PRICE = 0;
    const ORDER_BY_DISCOUNT = 1;
    const ORDER_BY_SALES = 2;
    const ORDER_BY_RATING = 3;
    const ORDER_BY_TIMESTAMP = 4;

    /** @var string|null */
    private $keyword;

    /** @var int */
    private $type;

    /** @var string[] */
    private $districts = [];

    /**
     * Payment.
     *
     * @var bool|null
     */
    private $online;

    /**
     * Confirmation.
     *
     * @var bool|null
     */
    private $manual;

    /** @var bool|null */
    private $delivery;

    /**
     * Scenic account.
     *
     * @var string|null
     */
    private $site;

    /**
     * Scenic tags.
     *
     * @var int[]
     */
    private $tags = [];

    /** @var Location|null */
    private $location;

    public function getKeyword(): ?string
    {
        return $this->keyword;
    }

    public function setKeyword(?string $keyword): self
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDistricts(): array
    {
        return $this->districts;
    }

    public function setDistricts(array $districts): self
    {
        $this->districts = $districts;

        return $this;
    }

    public function getOnline(): ?bool
    {
        return $this->online;
    }

    public function setOnline(?bool $online): self
    {
        $this->online = $online;

        return $this;
    }

    public function getManual(): ?bool
    {
        return $this->manual;
    }

    public function setManual(?bool $manual): self
    {
        $this->manual = $manual;

        return $this;
    }

    public function getDelivery(): ?bool
    {
        return $this->delivery;
    }

    public function setDelivery(?bool $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): ProductFilter
    {
        $this->location = $location;

        return $this;
    }

    public function toParams()
    {
        return array_filter([
            'keyWord' => $this->keyword,
            'treeId' => $this->type,
            'cityName' => implode(',', $this->districts),
            'isPay' => null !== $this->online ? ($this->online ? 1 : 0) : null,
            'isConfirm' => null !== $this->manual ? ($this->manual ? 1 : 0) : null,
            'isExpress' => null !== $this->delivery ? ($this->delivery ? 1 : 0) : null,
            'viewId' => $this->site,
            'tagIds' => implode(',', $this->tags),
            'location' => (string) $this->location,
        ], function ($x) {
            return !in_array($x, [null, ''], true);
        });
    }
}
