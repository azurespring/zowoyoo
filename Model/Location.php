<?php

namespace AzureSpring\Zowoyoo\Model;

class Location
{
    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /** @var int */
    private $distance;

    public function __construct(float $latitude, float $longitude, int $distance = 2000)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->distance = $distance;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getDistance(): int
    {
        return $this->distance;
    }

    public function __toString()
    {
        return implode(',', [$this->longitude, $this->latitude, $this->distance]);
    }
}
