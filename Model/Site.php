<?php

namespace AzureSpring\Zowoyoo\Model;

class Site
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $address;

    /** @var float|null */
    private $latitude;

    /** @var float|null */
    private $longitude;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }
}
