<?php

namespace AzureSpring\Zowoyoo\Model;

class DateValidity extends Validity
{
    /** @var \DateTimeImmutable */
    private $date;

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function getValidThru(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function prune()
    {
        $this->date = $this->date->setTime(0, 0);

        return $this;
    }
}
