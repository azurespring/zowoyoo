<?php

namespace AzureSpring\Zowoyoo\Model;

class Order
{
    const STATE_WAITING = 0;
    const STATE_ACKNOWLEDGED = 1;
    const STATE_DONE = 2;

    /** @var string */
    private $id;

    /** @var float */
    private $subtotal;

    /** @var int */
    private $state;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->subtotal;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }
}
