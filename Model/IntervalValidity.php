<?php

namespace AzureSpring\Zowoyoo\Model;

class IntervalValidity extends Validity
{
    /** @var \DateTimeImmutable[] */
    private $dates;

    /**
     * @return \DateTimeImmutable[]
     */
    public function getDates(): array
    {
        return $this->dates;
    }

    public function getValidFrom(): ?\DateTimeImmutable
    {
        return $this->dates[0];
    }

    public function getValidThru(): ?\DateTimeImmutable
    {
        return $this->dates[1];
    }

    public function prune()
    {
        $this->dates = array_map(
            function (\DateTimeImmutable $date) {
                return $date->setTime(0, 0);
            },
            $this->dates
        );

        return $this;
    }
}
