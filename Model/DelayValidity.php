<?php

namespace AzureSpring\Zowoyoo\Model;

class DelayValidity extends Validity
{
    /** @var int */
    private $delay = 0;

    /**
     * @return int
     */
    public function getDelay(): int
    {
        return $this->delay;
    }
}
