<?php

namespace AzureSpring\Zowoyoo\Model;

class Unit implements Squeezable
{
    private $data;

    public static function bind(string $type, ?string $name = null)
    {
        return "unit<{$type}, '{$name}'>";
    }

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function squeeze()
    {
        return $this->data;
    }
}
