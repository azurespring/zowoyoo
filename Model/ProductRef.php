<?php

namespace AzureSpring\Zowoyoo\Model;

class ProductRef
{
    const TYPE_TICKET = 0;
    const TYPE_PACKAGE = 1;
    const TYPE_PHYSICAL = 11;
    const TYPE_PRE_SALES = 12;

    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $image;

    /** @var float */
    private $costPrice;

    /** @var Site */
    private $site;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getCostPrice(): float
    {
        return $this->costPrice;
    }

    public function getSite(): Site
    {
        return $this->site;
    }
}
