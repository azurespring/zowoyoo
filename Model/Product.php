<?php

namespace AzureSpring\Zowoyoo\Model;

class Product extends ProductRef
{
    const FL_NAME = 'link_man';
    const FL_PHONE = 'link_phone';
    const FL_ID_NUMBER = 'link_credit_no';
    const FL_EMAIL = 'link_email';
    const FL_ADDRESS = 'link_address';

    /** @var \DateTimeImmutable */
    private $closeAt;

    /** @var int */
    private $minReservation;

    /** @var Validity */
    private $validity;

    /**
     * Require per seat data.
     *
     * @var bool
     */
    private $perSeat;

    /**
     * Manual acknowledgement.
     *
     * @var bool
     */
    private $manual;

    /**
     * Personal data fields (FL_XXX).
     *
     * @var string[]
     */
    private $fields;

    /**
     * @return \DateTimeImmutable
     */
    public function getCloseAt(): \DateTimeImmutable
    {
        return $this->closeAt;
    }

    /**
     * @return int
     */
    public function getMinReservation(): int
    {
        return $this->minReservation;
    }

    /**
     * @return Validity
     */
    public function getValidity(): Validity
    {
        return $this->validity;
    }

    public function getValidFrom(): ?\DateTimeImmutable
    {
        return $this->validity->getValidFrom();
    }

    public function getValidThru(): ?\DateTimeImmutable
    {
        return $this->validity->getValidThru();
    }

    /**
     * @return bool
     */
    public function isPerSeat(): bool
    {
        return $this->perSeat;
    }

    /**
     * @return bool
     */
    public function isManual(): bool
    {
        return $this->manual;
    }

    /**
     * @return string[]
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function prune()
    {
        $this->validity->prune();

        return $this;
    }
}
